/*** server.js ***/
const port = 3000;
const io = require("socket.io")(port);
const bcrypt = require('bcrypt');

console.log("Server is listening on port: %d", port);
var sqlite3 = require('sqlite3');
var db = new sqlite3.Database(':memory:');


io.of("/").on("connect", (socket) => {
    console.log("\nA client connected");
    
    socket.on("disconnect", (reason) => {
        console.log("\nA client disconnected, reason: %s", reason);
        console.log("Number of clients: %d", io.of('/').server.engine.clientsCount);
    });

    socket.on("broadcast", (data) => {
        console.log("\n%s", data);
        socket.broadcast.emit("broadcast", data);
    });

    socket.on("join", (data) => {
        console.log("\n%s", data);
        console.log("Nickname: ", data.sender, ", ID: ", socket.id);
        db_check_user(data.sender, data.password)
        db_save_user(data.sender, data.password);
        console.log("Number of clients: %d", io.of('/').server.engine.clientsCount);
        socket.nickname = data.sender;
        socket.broadcast.emit("join", data);
        
    });

    socket.on("list", (data) => {
        console.log("\n%s", data);
        var users = [];
        for (const [key, value] of io.of("/").sockets) {
            users.push(value.nickname);
        }
        socket.emit("list", {"sender": data.sender, "action": "list", "users": users});
    });

    socket.on("quit", (data) => {
        console.log("\n%s", data);
        socket.broadcast.emit("quit", data);
        socket.disconnect(true);
    });
    socket.on("trace", () => {
        console.log("\n=============== Trace ===============");
        console.log(io.of("/"));
    });    

    socket.on("send", (data) => {
        console.log("\n%s", data);
        
        var socket_id = null;
        for (const [key, value] of io.of("/").sockets) {
            if (data.receiver.toLowerCase() === value.nickname) {
                socket_id = key;
            }
        }
        if (socket_id !== null) {
            io.of("/").to(socket_id).emit("send", data);
        }
    }); 

    socket.on("join_group", (data) => {
        console.log("\n%s", data);
        socket.join(data.group);
        console.log("Group: ", data.group, ", Joined: ", data.sender);
        io.of("/").to(data.group).emit("join_group", data);
    });    
    socket.on("broadcast_group", (data) => {
        console.log("\n%s", data);
        socket.to(data.group).emit("broadcast_group", data);
        if (undefined === io.of("/").room_messages) {
            io.of("/").room_messages = {};
        }
        if (undefined === io.of("/").room_messages[data.group]) {
            io.of("/").room_messages[data.group] = [];
        }
        io.of("/").room_messages[data.group].push(data.msg);
        db_save_message(data.group, data.sender, data.msg);
    });    
    socket.on("list_members_group", (data) => {
        console.log("\n%s", data);
        var socket_ids;
        var members = [];
        for (const [key, value] of io.of("/").adapter.rooms) {
            if (key === data.group) {
                socket_ids = value;
            }
        }
        // console.log("\n%s", socket_ids);
        // socket_ids.forEach( (socket_id) => {
        //     const socket_in_room = io.of("/").sockets.get(socket_id);
        //     members.push(socket_in_room.nickname);
        // });
        socket_ids.forEach(function(socket_id) {
            const socket_in_room = io.of("/").sockets.get(socket_id);
            members.push(socket_in_room.nickname);
        });

        socket.emit("list_members_group", {"sender": data.sender, "action": "list_members_group", "group": data.group, "members": members});
    });

    // socket.on("list_messages_group", (data) => {
    //     console.log("\n%s", data);
    //     var msgs = io.of("/").room_messages[data.group];
    //     socket.emit("list_messages_group", {"sender": data.sender, "action": "list_messages_group", "group": data.group, "msgs": msgs});
    // });
    
    socket.on("list_messages_group", () => {
        console.log("\n%s", data);
        db.serialize(() => {
            db.all("SELECT msg FROM room_messages WHERE thegroup = ?", [data.group], (err, rows) => {
                var msgs = [];

                if (err) {
                    throw err;
                }
                rows.forEach((row) => {
                    console.log("Got a message from the database: " + row.msg);
                    msgs.push(row.msg);
                });
                
                socket.emit("list_messages_group", {"sender": data.sender, "action": "list_messages_group", "group": data.group, "msgs": msgs});
            });
        });
     });

    socket.on("list_groups", (data) => {
        console.log("\n%s", data);
        var groups = [];
        for (const [key, value] of io.of("/").adapter.rooms) {
            if (false === value.has(key)) {
                groups.push(key);
            }
        }
        socket.emit("list_groups", {"sender": data.sender, "action": "list_groups", "groups": groups});
    });
    socket.on("leave_group", (data) => {
        console.log("\n%s", data);
        
        socket.leave(data.group);
        console.log("Group: ", data.group, ", Left: ", data.sender);
        io.of("/").to(data.group).emit("leave_group", data);
    });

});

function db_save_message(group, sender, msg) {
    db.serialize(() => {        
        db.run("CREATE TABLE IF NOT EXISTS room_messages(thegroup TEXT, sender TEXT, msg TEXT)", function(err) {
            if (err) {
                throw err;
            }
        });

        db.run("INSERT INTO room_messages(thegroup, sender, msg) VALUES(?,?,?)", [group, sender, msg], function(err) {
            if (err) {
                throw err;
            }
            console.log("Saved the message to the database, rowid: " + this.lastID);
        });
    });
}

function db_save_user(sender, password) {
    const saltRounds = 10;
    const hash_password = bcrypt.hashSync(password, saltRounds);
    db.serialize(() => {        
        // db.run("CREATE TABLE IF NOT EXISTS user_table(sender TEXT, password TEXT)", function(err) {
        //     if (err) {
        //         throw err;
        //     }
        // });

        db.run("INSERT INTO user_table(sender, password) VALUES(?,?)", [sender, hash_password], function(err) {
            if (err) {
                throw err;
            }
            console.log("Saved the user to the database, rowid: " + this.lastID);
        });
    });
}
function db_check_user(sender, password) {
    db.serialize(() => {        
        db.run("CREATE TABLE IF NOT EXISTS user_table(sender TEXT, password TEXT)", function(err) {
            if (err) {
                throw err;
            }
        });
        db.each("SELECT * FROM user_table WHERE sender = ?", [sender], (err, rows) => {
            
            if (err) {
                throw err;
            }
        

            if (bcrypt.compareSync(password, rows.password)){
                console.log(sender + " is in"); 
            }


            });
    });
}
