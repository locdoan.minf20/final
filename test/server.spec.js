/* test/server.spec.js */
// Load the 'chai' module with the 'expect' style
const expect = require("chai").expect;
// Load the 'socket.io-client' module
const io = require("socket.io-client");

var num_tests = 1;
describe('----Testing server in the chat', function() {
  var socket_1, socket_2;
  var username_1 = "ted";
  var username_2 = "roni";
  var options = { "force new connection": true };
  beforeEach(function (done) {
    // This would set a timeout of 3000ms only for this hook
    this.timeout(3000);
    console.log(">> Test #" + (num_tests++));
    socket_1 = io("http://localhost:3000", options);
    socket_2 = io("http://localhost:3000", options);
    socket_1.on("connect", function() {
      console.log("socket_1 connected");
      socket_1.emit("join", {"sender": username_1, "action": "join"});
    });
    socket_2.on("connect", function() {
      console.log("socket_2 connected");
      socket_2.emit("join", {"sender": username_2, "action": "join"});
    });
    setTimeout(done, 500); // Call done() function after 500ms
  }); // End beforeEach()
  afterEach(function (done) {
    // This would set a timeout of 2000ms only for this hook
    this.timeout(2000);
    socket_1.on("disconnect", function() {
      console.log("socket_1 disconnected");
    });
    socket_2.on("disconnect", function() {
      console.log("socket_2 disconnected\n");
    });
    socket_1.disconnect();
    socket_2.disconnect();
    setTimeout(done, 500); // Call done() function after 500ms
  }); // End afterEach()

  it('Notify that a user joined the chat', function(done) {
    socket_1.emit("join", {"sender": username_1, "action": "join"});
    socket_2.on("join", function(data) {
      expect(data.sender).to.equal(username_1);
      done();
    });
  });

  it('Broadcast a message to others in the chat', function(done) {
    var msg_hello = "hello socket_2";
    socket_1.emit("broadcast", {"sender": username_1, "action": "broadcast", "msg": msg_hello});
    socket_2.on("broadcast", function(data) {
      expect(data.msg).to.equal(msg_hello);
      done();
    });
  });

  it('List all users in the chat', function(done) {
    socket_1.emit("list", {"sender": username_1, "action": "list"});
    socket_1.on("list", function(data) {
      expect(data.users).to.be.an('array').that.includes(username_1);
      expect(data.users).to.be.an('array').that.includes(username_2);
      done();
    });
  });

  it('A user quit the chat', function(done) {
    socket_1.emit("quit", {"sender": username_1, "action": "quit"});
    socket_2.emit("list", {"sender": username_2, "action": "list"});
    socket_2.on("list", function(data) {
      expect(data.users).to.be.an('array').that.not.includes(username_1);
      expect(data.users).to.be.an('array').that.includes(username_2);
      done();
    });
  });

  it('Notify that a user quit the chat', function(done) {
    socket_1.emit("quit", {"sender": username_1, "action": "quit"});
    socket_2.on("quit", function(data) {
      expect(data.sender).to.equal(username_1);
      done();
    });
  });

  it('Send a private message to a user', function(done) {
    var msg_hello = "hello socket_2";
    socket_1.emit("send", {"sender": username_1, "action": "send", "receiver": username_2, "msg": msg_hello});
    socket_2.on("send", function(data) {
      expect(data.receiver).to.equal(username_2);
      expect(data.msg).to.equal(msg_hello);
      done();
    });
  });
  
}); // End describe()

describe('----Testing server in a group', function() {
    var socket_1, socket_2;
    var username_1 = "ted";
    var username_2 = "roni";
    var group_name = "doctors"; 
    var options = { "force new connection": true };
    beforeEach(function (done) {
      // This would set a timeout of 3000ms only for this hook
      this.timeout(3000);
      console.log(">> Test #" + (num_tests++));
      socket_1 = io("http://localhost:3000", options);
      socket_2 = io("http://localhost:3000", options);
      socket_1.on("connect", function() {
        console.log("socket_1 connected");
        socket_1.emit("join", {"sender": username_1, "action": "join"});
        socket_1.emit("join_group", {"sender": username_1, "action": "join_group", "group": group_name});
      });
      socket_2.on("connect", function() {
        console.log("socket_2 connected");
        socket_2.emit("join", {"sender": username_2, "action": "join"});
        socket_2.emit("join_group", {"sender": username_2, "action": "join_group", "group": group_name});
      });
      setTimeout(done, 500); // Call done() function after 500ms
    }); // End beforeEach()
    afterEach(function (done) {
      // This would set a timeout of 2000ms only for this hook
      this.timeout(2000);
      socket_1.on("disconnect", function() {
        console.log("socket_1 disconnected");
      });
      socket_2.on("disconnect", function() {
        console.log("socket_2 disconnected\n");
      });
      socket_1.disconnect();
      socket_2.disconnect();
      setTimeout(done, 500); // Call done() function after 500ms
    }); // End afterEach()


    it('Notify that a user joined a group', function(done) {
        socket_1.emit("join_group", {"sender": username_1, "action": "join_group", "group": group_name});
        socket_1.on("join_group", function(data) {
          expect(data.sender).to.equal(username_1);
          expect(data.group).to.equal(group_name);
          done();
        });
      });

      it('Broadcast a message to a group', function(done) {
        var msg_hello = "hello socket_2";
        socket_1.emit("broadcast_group", {"sender": username_1, "action": "broadcast_group", "group": group_name, "msg": msg_hello});
        socket_2.on("broadcast_group", function(data) {
          expect(data.sender).to.equal(username_1);
          expect(data.group).to.equal(group_name);
          expect(data.msg).to.equal(msg_hello);
          done();
        });
      });      
      it('List all clients that are inside a group', function(done) {
        socket_1.emit("list_members_group", {"sender": username_1, "action": "list_members_group", "group": group_name});
        socket_1.on("list_members_group", function(data) {
          expect(data.sender).to.equal(username_1);
          expect(data.group).to.equal(group_name);
          expect(data.members).to.be.an('array').that.includes(username_1);
          expect(data.members).to.be.an('array').that.includes(username_2);
          done();
        });
      });

      it('List the existing groups', function(done) {
        socket_1.emit("list_groups", {"sender": username_1, "action": "list_groups"});
        socket_1.on("list_groups", function(data) {
          expect(data.sender).to.equal(username_1);
          expect(data.groups).to.be.an('array').that.includes(group_name);
          done();
        });
      });      
}); // End describe()